<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use DB;

class PaciTestController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

          
        }

        return $RES;
    }

     public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');
        $SQL = "select * from  paciente_mis_test_view ";
        $SQL = $SQL . " WHERE  (estado <> -1) ";
        $SQL = $SQL . "AND   (estado <> 3) ";
        $SQL = $SQL . " AND (id_paciente = $id_usuario) ";
        $OPER = " AND ";

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
            IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $SQL = $SQL . " $OPER ( $Columna  $Operador   '$Valor' )";
        }

        $resp = DB::select($SQL);


        return $resp;
    }



}
