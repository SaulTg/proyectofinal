<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\MisPacienteModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use DB;

class PsicPacienteController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
            case 'lista':
                $RES = $this->ListaPacientes($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');

        $SQL = "select * from  admin_pacientes_view ";
        $SQL = $SQL . " WHERE  (estado <> -1) ";
        $SQL = $SQL . " AND  (id_psicologo =    $id_usuario) ";
        $OPER = " AND ";

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
              IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $SQL = $SQL . " $OPER ( $Columna  $Operador   '$Valor' )";
        }

        $resp = DB::select($SQL);


        return $resp;
    }

    public function Agregar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Model = new UsuarioModel;
        $Model->id_rol = 3;
        $Model->dni_usu = $request->get('dni');
        $Model->nombre_usu = $request->get('nombre');
        $Model->apellido_usu = $request->get('apellido');
        $Model->correo_usu = $request->get('correo');
        $Model->direccion_usu = $request->get('direccion');
        $clave = $request->get('clave');
        if ($clave == "Default") {
            $clave = strtoupper(uniqid());
        } else {
            $Model->clave_usu = $clave;
        }
        $Model->clave_usu = $clave;
        $Model->estado_usu = $request->get('estado');
        $Model->save();

        //asiganmos un paciente a un psicologo
        $ModelP = new MisPacienteModel;
        $ModelP->id_psicologo = $id_usuario;
        $ModelP->id_paciente = $Model->id;
        $ModelP->estado_mis_pac = $request->get('estado');
        $ModelP->save();

        return response()->json([ "id" => $ModelP->id, "clave" => $Model->clave_usu]);
    }

    public function Modificar(Request $request) {
        $id_usuario = $request->get('id_usuario');

        $ID = $request->get('id');
        $ModelP = MisPacienteModel::find($ID);
        $ModelP->id_psicologo = $id_usuario;
        $ModelP->estado_mis_pac = $request->get('estado');
        $ModelP->update();
        $ModelU = UsuarioModel::find($ModelP->id_paciente);
        $ModelU->dni_usu = $request->get('dni');
        $ModelU->nombre_usu = $request->get('nombre');
        $ModelU->apellido_usu = $request->get('apellido');
        $ModelU->correo_usu = $request->get('correo');
        $ModelU->direccion_usu = $request->get('direccion');
        $clave = $request->get('clave');
        if ($clave == "Default") {
            $clave = strtoupper(uniqid());
        }
        $ModelU->estado_usu = $request->get('estado');
        $ModelU->update();

        return response()->json([ "id" => $ModelP->id, "clave" => $ModelU->clave_usu]);
        /* $ID = $request->get('id');
          $Model = UsuarioModel::find($ID);
          $Model->dni_usu = $request->get('dni');
          $Model->nombre_usu = $request->get('nombre');
          $Model->apellido_usu = $request->get('apellido');
          $Model->correo_usu = $request->get('correo');
          $Model->direccion_usu = $request->get('direccion');
          $clave = $request->get('clave');
          if ($clave == "Default") {
          $clave = strtoupper(uniqid());
          } else {
          $Model->clave_usu = $clave;
          }
          $Model->clave_usu = $clave;
          $Model->estado_usu = $request->get('estado');
          $Model->update();
          //asiganmos un paciente a un psicologo
          $ModelP = MisPacienteModel::where("id_paciente", $Model->id)->get();
          if (count($ModelP) != 0) {
          $ModelP[0]->id_psicologo = $request->get('id_psicologo');
          $ModelP[0]->estado_mis_pac = $request->get('estado');
          $ModelP[0]->save();
          }

          return response()->json([ "id" => $ModelP->id, "clave" => $Model->clave_usu]); */
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $ModelP = MisPacienteModel::find($ID);
        $ModelP->estado_mis_pac = -1;
        $ModelP->update();
        $ModelU = UsuarioModel::find($ModelP->id_paciente);
        $ModelU->estado_usu = -1;
        $ModelU->update();

        return response()->json($ID);
    }

    public function ListaPacientes(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $resp = \App\MisPacienteModel::select(
                        "tb_mis_pacientes.id as index"
                        , "u.dni_usu as value"
                )
                ->join("tb_usuario as u", 'u.id', '=', 'tb_mis_pacientes.id_paciente')
                ->where('estado_usu', "=", 1)
                ->where('id_psicologo', "=", $id_usuario)
                ->where('id_rol', 3)
                ->get();


        return $resp;
    }

}
