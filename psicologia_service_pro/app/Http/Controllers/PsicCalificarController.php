<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\AsignacionModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use DB;

class PsicCalificarController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'resultados':
                $RES = $this->MostrarTotal($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');

        $SQL = "select * from  asignacion_view  ";
        $SQL = $SQL . " WHERE (estado = 3) ";
        $OPER = " AND ";

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');

            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $SQL = $SQL . " $OPER ( $Columna  $Operador   '$Valor' )";
        }

        $resp = DB::select($SQL);


        return $resp;
    }

    public function MostrarTotal(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_paciente = $request->get('id_paciente');

        $SQL = "SELECT * FROM resultado_view";
        $SQL = $SQL . " WHERE (id_paciente = $id_paciente) ";

        $resp = DB::select($SQL);
        $c = count($resp);
        $MIN = 0;
        $MAX = 0;
        if ($c != 0) {
            $MIN = $resp[0]->fecha;
            $MAX = $resp[$c-1]->fecha;
        }

        return response()->json([ "min" => $MIN, "max" =>  $MAX, "data" => $resp]);
    }

}
