<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use Illuminate\Http\Request;


class LoginController extends Controller
{
   
    public function login(Request $request) {

         $correo = $request->correo;
        $clave = $request->clave;
        $usuario_logueado = UsuarioModel::select(
                        "tb_usuario.id", "id_rol as id_rol", "r.nombre_rol as rol", "nombre_usu as nombre", "apellido_usu as apellido"
                )
                ->join("tb_rol as r", "r.id", "id_rol")
                ->where('correo_usu', $correo)
                ->where('clave_usu', $clave)
                ->where('estado_usu', 1)
                ->first();
        switch (true) {
            case is_null($usuario_logueado):
                $usuario_logueado = 0;
                break;

            default:
                $usuario_logueado = json_encode($usuario_logueado);
                break;
        }

        return  $usuario_logueado;
    }


}
