<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use DB;

class PsicTestController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
            case 'lista':
                $RES = $this->ListaTest($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');
        $SQL = "select * from  test_view ";
        $SQL = $SQL . " WHERE  (estado <> -1) ";
        $SQL = $SQL . " AND (id_psicologo_creador = $id_usuario) ";
        $OPER = " AND ";

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
            IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $SQL = $SQL . " $OPER ( $Columna  $Operador   '$Valor' )";
        }

        $resp = DB::select($SQL);


        return $resp;
    }

    public function Agregar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Model = new TestModel;
        $Model->nombre_tes = $request->get('nombre');
        $Model->descripcion_tes = $request->get('descripcion');
        $Model->fecha_tes = $request->get('fecha');
        $Model->id_psicologo_creador = $id_usuario;
        $Model->estado_tes = $request->get('estado');
        $Model->save();
        return response()->json([ "id" => $Model->id]);
    }

    public function Modificar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $ID = $request->get('id');
        $Model = TestModel::find($ID);
        $Model->nombre_tes = $request->get('nombre');
        $Model->descripcion_tes = $request->get('descripcion');
        $Model->fecha_tes = $request->get('fecha');
        $Model->id_psicologo_creador = $id_usuario;
        $Model->estado_tes = $request->get('estado');
        $Model->update();
        return response()->json([ "id" => $Model->id]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $Model = TestModel::find($ID);
        $Model->estado_tes = -1;
        $Model->update();
        return response()->json($Model->id);
    }

    public function ListaTest(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $resp = TestModel::select(
                        "id as index"
                        , "nombre_tes as value"
                )
                ->where('estado_tes', "=", 1)
                ->where('id_psicologo_creador', "=", $id_usuario)
                ->get();
        return $resp;
    }

}
