<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use App\PreguntaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use App\RespuestaPacienteModel;
use DB;

class PsicoPreguntaContestadaController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_asignacion = $request->get('id_asignacion');
        $id_test = $request->get('id_test');
        $resp = \App\PreguntaModel::select(
                        "id as id"
                        , "id  as respuesta"
                        , "pregunta_det as pregunta"
                        , "estado_det as estado"
                        , DB::raw('0 as total')
        );
        //$resp = $resp->where('estado_det', "<>", -1);
        $resp = $resp->where('estado_det', 1);
        $resp = $resp->where('id_test', $id_test);

        $resp = $resp->get();
        $GlobalTotal = 0;
        foreach ($resp as $res) {

            $respuestas = \App\RespuestaPacienteModel::
                    select(
                            "r.id"
                            , "r.respuesta_res as respuesta"
                            , "r.calificacion_res as calificacion"
                            , "r.estado_res as estado"
                    )->join("tb_respuesta as r", "r.id", "tb_respuesta_pasiente.id_respuesta")
                    ->where("id_asignacion", $id_asignacion)
                    ->where("estado_res", "<>", -1)
                    ->where("r.id_detalle", $res->id)
                    ->where("estado_res_pas", 1)
                    ->get();
            $total = 0;
            foreach ($respuestas as $resps) {
                $total = $total + $resps->calificacion;
            }
            $res->total = $total;
            $GlobalTotal = $GlobalTotal + $total;
            $res->respuesta = $respuestas;
        }

        return response()->json([ "total" => $GlobalTotal, "data" => $resp]);
    }

}
