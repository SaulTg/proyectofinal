<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use App\PreguntaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use App\RespuestaPacienteModel;

class PaciPreguntaContestadaController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_asignacion = $request->get('id_asignacion');
        $id_test = $request->get('id_test');
        $resp = \App\PreguntaModel::select(
                        "id as id"
                        , "id  as respuesta"
                        , "pregunta_det as pregunta"
                        , "estado_det as estado"
        );
        //$resp = $resp->where('estado_det', "<>", -1);
        $resp = $resp->where('estado_det', 1);
        $resp = $resp->where('id_test', $id_test);

        $resp = $resp->get();
        foreach ($resp as $res) {
            $respuestas = \App\RespuestaModel::select(
                            "id"
                            , "respuesta_res as respuesta"
                            , "calificacion_res as calificacion"
                            , "estado_res as estado"
                            , "id as value"
                    )
                    ->where("id_detalle", $res->id)
                    ->where("estado_res", 1)
                    ->get();
            
            foreach ($respuestas as $respuesta) {
                $cont = \App\RespuestaPacienteModel::
                        where("id_asignacion", $id_asignacion)
                        ->where("id_respuesta", $respuesta->id)
                        ->where("estado_res_pas", 1)
                        ->get();
                if (count($cont)) {
                    $respuesta->value = true;
                } else {
                    $respuesta->value = false;
                }
            }
            $res->respuesta = $respuestas;
        }
        return $resp;
    }

}
