<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use App\PreguntaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use App\RespuestaPacienteModel;

class PaciPreguntaController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_test = $request->get('id_test');
        $resp = \App\PreguntaModel::select(
                        "id as id"
                        , "id  as respuesta"
                        , "pregunta_det as pregunta"
                        , "estado_det as estado"
        );
        //$resp = $resp->where('estado_det', "<>", -1);
        $resp = $resp->where('estado_det', 1);
        $resp = $resp->where('id_test', $id_test);

        $resp = $resp->get();
        foreach ($resp as $res) {
            $respuesta = \App\RespuestaModel::select(
                            "id"
                            , "respuesta_res as respuesta"
                            , "calificacion_res as calificacion"
                            , "estado_res as estado"
                           
                    )
                    ->where("id_detalle", $res->id)
                    ->where("estado_res", 1)
                    ->get();
            $res->respuesta = $respuesta;
        }
        return $resp;
    }

    public function Agregar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_asignacion = $request->get('id_asignacion');
        $ids_respuesta = $request->get('ids_respuesta');
        $ids_respuesta = explode(",", $ids_respuesta);
        $asignacion = \App\AsignacionModel::find($id_asignacion);
        $asignacion->estado_asig = 3;
        $asignacion->update();
        $Models = RespuestaPacienteModel::where("id_asignacion", $id_asignacion)->get();
        foreach ($Models as $Model) {
           $Model->delete();
        }

        foreach ($ids_respuesta as $id_respuesta) {
            if ($id_respuesta != null) {

                $Model = new RespuestaPacienteModel();
                $Model->id_asignacion = $id_asignacion;
                $Model->id_respuesta = $id_respuesta;
                $Model->estado_res_pas = 1;
                $Model->save();
            }
        }

        return response()->json([ "estado" => 1]);
    }

}
