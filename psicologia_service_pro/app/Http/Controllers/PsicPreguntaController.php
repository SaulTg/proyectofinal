<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use App\PreguntaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;

class PsicPreguntaController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
            case 'lista':
                $RES = $this->ListaTest($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_test = $request->get('id_test');
        $Filtros = $request->get('Filtros');
        $resp = \App\PreguntaModel::select(
                        "id as id"
                        , "id  as respuesta"
                        , "pregunta_det as pregunta"
                        , "estado_det as estado"
        );
        //$resp = $resp->where('estado_det', "<>", -1);
          $resp = $resp->where('estado_det', "<>", -1);
        $resp = $resp->where('id_test', $id_test);

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
            IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Columna = $Columna . "_det";
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $resp = $resp->where($Columna, $Operador, $Valor);
        }
        $resp = $resp->get();
        foreach ($resp as $res) {

            $respuesta = \App\RespuestaModel::select(
                            "id"
                            , "respuesta_res as respuesta"
                            , "calificacion_res as calificacion"
                            , "estado_res as estado"
                    )
                    ->where("id_detalle", $res->id)
                  //  ->where("estado_res", "<>", -1)
                    ->where("estado_res", 1)
                    ->get();
            $res->respuesta = $respuesta;
        }
        return $resp;
    }

    public function Agregar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Model = new PreguntaModel;
        $Model->id_test = $request->get('id_test');
        $Model->pregunta_det = $request->get('pregunta');
        $Model->estado_det = $request->get('estado');
        $Model->save();
        return response()->json([ "id" => $Model->id]);
    }

    public function Modificar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $ID = $request->get('id');
        $Model = PreguntaModel::find($ID);
        $Model->pregunta_det = $request->get('pregunta');
        $Model->estado_det = $request->get('estado');
        $Model->update();
        return response()->json([ "id" => $Model->id]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $Model = PreguntaModel::find($ID);
        $Model->estado_det = -1;
        $Model->update();
        return response()->json($Model->id);
    }

    /* public function ListaTest(Request $request) {
      $resp = \Tests::select(
      "id as index"
      , "nombre_tes as value"
      )
      ->where('estado_tes', "=", 1)
      ->get();
      return $resp;
      } */
}
