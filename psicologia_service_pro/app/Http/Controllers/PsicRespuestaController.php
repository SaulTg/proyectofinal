<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\TestModel;
use App\RespuestaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;

class PsicRespuestaController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $id_detalle = $request->get('id_detalle');
        $Filtros = $request->get('Filtros');
        $resp = \App\RespuestaModel::select(
                        "id as id"
                        , "id_detalle"
                        , "respuesta_res as respuesta"
                        , "calificacion_res as calificacion"
                        , "estado_res as estado"
        );
        $resp = $resp->where('estado_res', "<>", -1);
        $resp = $resp->where('id_detalle', $id_detalle);

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
            IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Columna = $Columna . "_res";
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $resp = $resp->where($Columna, $Operador, $Valor);
        }
        $resp = $resp->get();
          return $resp;
      
    }

    public function Agregar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Model = new RespuestaModel;
        $Model->id_detalle = $request->get('id_detalle');
        $Model->respuesta_res = $request->get('respuesta');
        $Model->calificacion_res = $request->get('calificacion');
        $Model->estado_res = $request->get('estado');
        $Model->save();
        return response()->json([ "id" => $Model->id]);
    }

    public function Modificar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $ID = $request->get('id');
        $Model = RespuestaModel::find($ID);
        $Model->respuesta_res = $request->get('respuesta');
        $Model->calificacion_res = $request->get('calificacion');
        $Model->estado_res = $request->get('estado');
        $Model->update();
        return response()->json([ "id" => $Model->id]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $Model = RespuestaModel::find($ID);
        $Model->estado_res = -1;
        $Model->update();
        return response()->json($Model->id);
    }

}
