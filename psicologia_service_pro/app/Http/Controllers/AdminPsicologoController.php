<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;

class AdminPsicologoController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
            case 'lista':
                $RES = $this->ListaPsicologo($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');
        $resp = UsuarioModel::select(
                        "tb_usuario.id as id"
                        , "tb_usuario.dni_usu as dni"
                        , "tb_usuario.nombre_usu as nombre"
                        , "tb_usuario.apellido_usu as apellido"
                        , "tb_usuario.correo_usu as correo"
                        , "tb_usuario.direccion_usu as direccion"
                        , "tb_usuario.clave_usu as clave"
                        , "tb_usuario.estado_usu as estado"
                )
                ->join("tb_rol as r", "r.id", "tb_usuario.id_rol")
                ->where('tb_usuario.estado_usu', "<>", -1);
        $resp = $resp->where('tb_usuario.id_rol', 2);

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
                 IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Columna = "tb_usuario." . $Columna . "_usu";
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $resp = $resp->where($Columna, $Operador, $Valor);
        }
        $resp = $resp->get();
        return $resp;
    }

    public function Agregar(Request $request) {
        // $id_usuario = $request->get('id_usuario');
        $Model = new UsuarioModel;
        $Model->id_rol = 2;
        $Model->dni_usu = $request->get('dni');
        $Model->nombre_usu = $request->get('nombre');
        $Model->apellido_usu = $request->get('apellido');
        $Model->correo_usu = $request->get('correo');
        $Model->direccion_usu = $request->get('direccion');
        $clave = $request->get('clave');
        if ($clave == "Default") {
            $clave = strtoupper(uniqid());
        } else {
            $Model->clave_usu = $clave;
        }
        $Model->clave_usu = $clave;
        $Model->estado_usu = $request->get('estado');
        $Model->save();
        return response()->json([ "id" => $Model->id, "clave" => $Model->clave_usu]);
    }

    public function Modificar(Request $request) {
        // $id_usuario = $request->get('id_usuario');
        $ID = $request->get('id');
        $Model = UsuarioModel::find($ID);
        $Model->dni_usu = $request->get('dni');
        $Model->nombre_usu = $request->get('nombre');
        $Model->apellido_usu = $request->get('apellido');
        $Model->correo_usu = $request->get('correo');
        $Model->direccion_usu = $request->get('direccion');
        $clave = $request->get('clave');
        if ($clave == "Default") {
            $clave = strtoupper(uniqid());
        }
        $Model->clave_usu = $clave;
        $Model->estado_usu = $request->get('estado');
        $Model->update();
        return response()->json([ "id" => $Model->id, "clave" => $Model->clave_usu]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $Model = UsuarioModel::find($ID);
        $Model->estado_usu = -1;
        $Model->update();
        return response()->json($Model->id);
    }

    public function ListaPsicologo(Request $request) {
        $resp = UsuarioModel::select(
                        "id as index"
                        , "dni_usu as value"
                )
                ->where('estado_usu', "=", 1)
                ->where('id_rol', 2)
                ->get();
        return $resp;
    }

}
