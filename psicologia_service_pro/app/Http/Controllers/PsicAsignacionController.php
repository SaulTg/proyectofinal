<?php

namespace App\Http\Controllers;

use App\UsuarioModel;
use App\AsignacionModel;
use Illuminate\Http\Request;
use App\Http\Controllers\UTILITARIOS;
use DB;

class PsicAsignacionController extends Controller {

    public function CRUD(Request $request) {
        $OPER = $request->get('oper');
        $RES = [];

        switch ($OPER) {
            case 'show':
                $RES = $this->Mostrar($request);
                break;

            case 'add':
                $RES = $this->Agregar($request);
                break;

            case 'edi':
                $RES = $this->Modificar($request);
                break;

            case 'del':
                $RES = $this->Eliminar($request);
                break;
        }

        return $RES;
    }

    public function Mostrar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $Filtros = $request->get('Filtros');

        $SQL = "select * from  asignacion_view  ";
        
           $SQL = $SQL . " WHERE  (estado <> -1) ";
         $SQL = $SQL . "AND   (estado <> 3) ";
        $OPER = " AND ";

        if ($Filtros == "true") {
            $Columna = strtolower($request->get('Columna'));
            $Valor = $request->get('Valor');
            IF ($Columna == "estado") {
                $Valor = strtolower($Valor);
                switch ($Valor) {
                    case "activado":
                        $Valor = 1;
                        break;
                    case "desactivado":
                        $Valor = 0;
                        break;
                    default :
                        $Valor = -2;
                        break;
                }
            }
            $Operador = UTILITARIOS::Operadores($request->get('Operador'));
            $SQL = $SQL . " $OPER ( $Columna  $Operador   '$Valor' )";
        }

        $resp = DB::select($SQL);


        return $resp;
    }

    public function Agregar(Request $request) {
        $ID = 0;
        $id_usuario = $request->get('id_usuario');
        $Model = new AsignacionModel();
        $id_paciente = $request->get('id_paciente');
        $id_test = $request->get('id_test');
        $existe = AsignacionModel::where("id_mis_pasiente", $id_paciente)
                ->where("id_test", $id_test)
                ->where("estado_asig", "<>", -1)
                 ->where("estado_asig","<>",  3)
                ->get();
        if (count($existe) == 0) {
            $Model->codigo_asig = strtoupper(uniqid());
            $Model->id_mis_pasiente = $id_paciente;
            $Model->id_test = $id_test;
            $Model->fecha_asig = $request->get('fecha');
            $Model->estado_asig = $request->get('estado');
            $Model->save();
            $ID = $Model->id;
        } else {
            $ID = -1;
        }

        return response()->json([ "id" => $ID]);
    }

    public function Modificar(Request $request) {
        $id_usuario = $request->get('id_usuario');
        $ID = $request->get('id');
        $id_paciente = $request->get('id_paciente');
        $id_test = $request->get('id_test');

        $Model = AsignacionModel::find($ID);
        $existe = AsignacionModel::where("id_mis_pasiente", $id_paciente)
                ->where("id_test", $id_test)
                ->where("id", "<>", $ID)
                ->where("estado_asig", "<>", -1)
                ->where("estado_asig", "<>", 3)
                ->get();
        if (count($existe) == 0) {
            $Model->id_mis_pasiente = $request->get('id_paciente');
            $Model->id_test = $request->get('id_test');
            $Model->fecha_asig = $request->get('fecha');
            $Model->estado_asig = $request->get('estado');
            $Model->update();
            $ID = $Model->id;
        } else {
            $ID = -1;
        }
        return response()->json([ "id" => $ID]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->get('id');
        $Model = AsignacionModel::find($ID);
        $Model->estado_asig = -1;
        $Model->update();
        return response()->json($Model->id);
    }

}
//NO SE PUEDE ASIGNAR UN TEST REPETIDO AL PACIENTE , CUANDO ESTE RESPONDA EL TEST HAY SI SE PUEDE ENVIAAR NUEVAMENTE UNO IDENTICO
//PUEDE ASIGNAR UN TEST NUEVAMENTE SI ESTE YA FUE REALIZADO O CALIFICADO