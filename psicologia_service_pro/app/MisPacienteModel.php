<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MisPacienteModel extends Model {

    protected $table = 'tb_mis_pacientes';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
