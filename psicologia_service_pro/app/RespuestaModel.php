<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaModel extends Model {

    protected $table = 'tb_respuesta';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
