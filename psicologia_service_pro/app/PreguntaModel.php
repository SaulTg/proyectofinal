<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaModel extends Model {

    protected $table = 'tb_detalle';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
