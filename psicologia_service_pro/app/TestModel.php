<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model {

    protected $table = 'tb_test';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
