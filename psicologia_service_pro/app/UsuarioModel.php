<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioModel extends Model {

    protected $table = 'tb_usuario';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
