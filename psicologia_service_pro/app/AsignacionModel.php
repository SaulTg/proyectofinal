<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignacionModel extends Model {

    protected $table = 'tb_asignacion';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
