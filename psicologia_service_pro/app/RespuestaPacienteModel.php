<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaPacienteModel extends Model {

    protected $table = 'tb_respuesta_pasiente';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
