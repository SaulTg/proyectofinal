-- MOSTRAR PACIENETES

DROP VIEW IF EXISTS   admin_pacientes_view;
CREATE  VIEW  admin_pacientes_view  AS
SELECT 
mp .id,
psic.id as id_psicologo,
psic.dni_usu as psicologo,
 pac.id as id_paciente, 
 pac.dni_usu as dni, 
 pac.nombre_usu as nombre,
 pac.apellido_usu as apellido,
 pac.correo_usu as correo,
pac. clave_usu as clave,
pac. direccion_usu as direccion,
mp .estado_mis_pac as estado
FROM tb_mis_pacientes as mp 

LEFT JOIN  tb_usuario  as psic
ON( mp.id_psicologo = psic.id)
 
LEFT JOIN  tb_usuario  as pac
ON( mp.id_paciente = pac.id);




SELECT * FROM admin_pacientes_view;




-- MOSTRAR ASIGNACION

DROP VIEW IF EXISTS   asignacion_view;
CREATE  VIEW  asignacion_view AS

SELECT 
a.id as id,

a.fecha_asig as fecha,
a.codigo_asig as codigo,
a.estado_asig as estado,
a.id_mis_pasiente as id_paciente,
a.id_test as id_test,

t.nombre_tes test,
t.descripcion_tes descripcion,
p.id_psicologo ,
p.dni ,
p.nombre,
p.apellido

FROM tb_asignacion AS a


INNER JOIN admin_pacientes_view as p
ON (a.id_mis_pasiente =p.id )

INNER JOIN tb_test as t
ON (a.id_test =t.id )

;
SELECT * FROM asignacion_view;


-- MOSTRAR DEALLES ACTIVOS
DROP VIEW IF EXISTS   detalle_view;
CREATE  VIEW  detalle_view  AS
SELECT * FROM tb_detalle WHERE (estado_det = 1);

SELECT * FROM detalle_view;






-- MOSTRAR TEST CON NUMERO DE PREGUNTAS
DROP VIEW IF EXISTS   test_view;
CREATE  VIEW    test_view  AS

SELECT 
t.id,
t.fecha_tes as fecha ,
t.nombre_tes as nombre,
t.descripcion_tes as descripcion ,
t.estado_tes as estado ,
t.id_psicologo_creador as id_psicologo_creador ,
COUNT(DISTINCT d.id) as cantidad_preguntas
FROM tb_test AS t
LEFT JOIN detalle_view AS d
ON( t.id = d.id_test )
GROUP BY
t.id,
t.fecha_tes ,
t.nombre_tes ,
t.descripcion_tes ,
t.estado_tes,
t.id_psicologo_creador
  ;

select * from  test_view;



-- MOSTRART TEST ASIGNADOAS A UN PACIENTE
DROP VIEW IF EXISTS   paciente_mis_test_view;
CREATE  VIEW    paciente_mis_test_view  AS

SELECT 
a.id,
a.id_mis_pasiente,
a.id_test,
a.codigo_asig as codigo,
a.fecha_asig as fecha,
a.estado_asig as estado,
t.nombre as nombre,
t.descripcion,
t.cantidad_preguntas,
pac.id_paciente
FROM tb_asignacion a

LEFT JOIN test_view as t
ON(  a.id_test = t.id)

INNER JOIN  tb_mis_pacientes AS pac
ON( a.id_mis_pasiente = PAC.id);
SELECT *  FROM paciente_mis_test_view;
 





-- MOSTRAR CALIFICACIONES DE PACIENTE
-- MOSTRART resultado de paciente
DROP VIEW IF EXISTS    resultado_view;
CREATE  VIEW    resultado_view  AS

SELECT 
a.id_mis_pasiente  as id_paciente, 
a.codigo_asig as codigo, 
a.fecha_asig as fecha, 
 t.nombre_tes  as test, 

SUM(r.calificacion_res ) as puntos 
FROM tb_asignacion as a

INNER  JOIN tb_respuesta_pasiente  as  rp 

ON ( rp.id_asignacion = a.id)

INNER JOIN tb_respuesta  as  r
ON ( rp.id_respuesta = r.id)

INNER JOIN tb_test  as  t
ON ( a.id_test = t.id)
WHERE  (a.estado_asig = 3)

GROUP BY
a.id,
a.id_mis_pasiente,
a.codigo_asig ,
a.fecha_asig ,
 t.nombre_tes 
 
  order by fecha ASC
; 
 

  SELECT * FROM resultado_view  ;