<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//GLOBAL
Route::post('app/login','LoginController@login');

//ADMINISTRADOR
Route::post('app/Admin/Psicologo','AdminPsicologoController@CRUD');
Route::post('app/Admin/Paciente','AdminPacienteController@CRUD');


//PSICOLOGO
Route::post('app/Psic/Paciente','PsicPacienteController@CRUD');
Route::post('app/Psic/Test','PsicTestController@CRUD');
Route::post('app/Psic/Pregunta','PsicPreguntaController@CRUD');
Route::post('app/Psic/Respuesta','PsicRespuestaController@CRUD');

Route::post('app/Psic/Asignacion','PsicAsignacionController@CRUD');
Route::post('app/Psic/Calificar','PsicCalificarController@CRUD');
Route::post('app/Psico/PreguntaContestada','PsicoPreguntaContestadaController@CRUD');
//PACIETE
Route::post('app/Paci/Test','PaciTestController@CRUD');
Route::post('app/Paci/TestContestado','PaciTestContestadoController@CRUD');
Route::post('app/Paci/Pregunta','PaciPreguntaController@CRUD');
Route::post('app/Paci/PreguntaContestada','PaciPreguntaContestadaController@CRUD');


//PRUEBAS
Route::get('ver','Prueba@ver');